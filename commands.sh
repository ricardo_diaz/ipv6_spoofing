ip netns add Alice
ip netns add Bob
ip netns add Frederic

ip link add spoof0 type bridge

ip link add vetha0 address 00:00:00:00:00:a0 type veth peer name vetha1 address 00:00:00:00:00:a1
ip link add vethb0 address 00:00:00:00:00:b0 type veth peer name vethb1 address 00:00:00:00:00:b1
ip link add vethf0 address 00:00:00:00:00:f0 type veth peer name vethf1 address 00:00:00:00:00:f1

ip link set dev vetha0 netns Alice
ip link set dev vethb0 netns Bob
ip link set dev vethf0 netns Frederic

ip link set vetha1 master spoof0
ip link set vethb1 master spoof0
ip link set vethf1 master spoof0

ip netns exec Alice ip link set dev vetha0 up
ip netns exec Bob ip link set dev vethb0 up
ip netns exec Frederic ip link set dev vethf0 up
ip netns exec Alice ip link set dev lo up
ip netns exec Bob ip link set dev lo up
ip netns exec Frederic ip link set dev lo up

ip link set dev vetha1 up
ip link set dev vethb1 up
ip link set dev vethf1 up
ip link set dev spoof0 up

ip netns exec Alice ip -6 address add 2001:a:b:c::a0/64 dev vetha0
ip netns exec Bob ip -6 address add 2001:a:b:c::b0/64 dev vethb0
ip netns exec Frederic ip -6 address add 2001:a:b:c::f0/64 dev vethf0

ip netns exec Frederic sysctl -w net.ipv6.conf.all.forwarding=1
