#!/usr/bin/env python

from scapy.all import *
from scapy.layers.inet6 import *


ether = Ether(src='00:00:00:00:00:f0', dst='00:00:00:00:00:a0')
ipv6=IPv6(src='2001:a:b:c::f0', dst='fe80::200:ff:fe00:a0')
na=ICMPv6ND_NA(tgt='2001:a:b:c::b0', R=0)
lla=ICMPv6NDOptDstLLAddr(lladdr='00:00:00:00:00:f0')

(ether/ipv6/na/lla).display()
sendp(ether/ipv6/na/lla, iface="vethf0", loop=1, inter=5)
